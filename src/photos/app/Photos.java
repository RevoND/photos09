package photos.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import photos.controller.PhotosController;

import java.io.IOException;

/**
 * @author Arjun Srivastav
 * Entry point for the Photos application.
 */
public class Photos extends Application {

    /**
     * Entry point for javaFX application/
     * @param primaryStage The stage associated with this controller.
     */
    @Override
    public void start(Stage primaryStage) {


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/view/loginScreen.fxml"));

        Pane root;
        try {
            root = loader.load();
        } catch (IOException e) {
            System.out.println("Unable to load login pane.");
            return;
        }

        PhotosController photosController = loader.getController();
        photosController.start(primaryStage);

        Scene loginScene = new Scene(root);

        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Application entry point.
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
