package photos.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import photos.model.Dialogues;
import photos.model.Photo;
import photos.model.User;

import java.io.FileWriter;
import java.io.IOException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Noe Duran
 * This class represents the controller for the Admin Panel.
 */
public class AdminPanelController {
    /**
     * the stage associated with this controller.
     */
    Stage mainStage;

    /**
     * The button labelled "add user"
     */
    @FXML Button addUserButton;
    /**
     * The button labelled "delete user"
     */
    @FXML Button deleteUserButton;
    /**
     * The button labelled "log out"
     */
    @FXML Button logoutButton;
    /**
     * The ListView that contains a list of all the users (mapped by username).
     */
    @FXML ListView<String> userList;

    /**
     * Action executed when the logout button is pressed.
     * @param ae The actionevent associated with the button press.
     * @throws IOException If the FXML file associated with the previous view cannot be found or loaded.
     */
    @FXML
    private void logout(ActionEvent ae) throws IOException {
        Stage mainStage = getMainStage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/view/loginScreen.fxml"));
        Pane newPane = loader.load();
        PhotosController loginScreen = loader.getController();
        loginScreen.start(mainStage);

        Scene loginScene = new Scene(newPane);

        mainStage.setScene(loginScene);
        mainStage.setTitle("Login");
        mainStage.setResizable(false);
        mainStage.show();
    }

    /**
     * This method runs right before the scene using this controller loads in.
     * @param mainStage The stage associated with this controller.
     */
    public void start(Stage mainStage) {
        setMainStage(mainStage);
        updateUserList();
    }

    /**
     * Adds a user to the list of users.
     * @param actionEvent The actionevent associated with the button press.
     * @throws IOException If the FXML file associated with the previous view cannot be found or loaded.
     */
    public void addUser(ActionEvent actionEvent) throws IOException {
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("Add a User");
        userDialog.setContentText("Username:");

        User newUser = new User(userDialog.showAndWait().get());
        try {
            File newUserSer = new File("src/photos/users/"+newUser.getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(newUser);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }

        userList.getItems().add(newUser.getUsername());

    }

    /**
     * Deletes a user from the list of users.
     * @param actionEvent The actionevent associated with this button press.
     * @throws IOException If the FXML file associated with the previous view cannot be found or loaded.
     */
    public void deleteUser(ActionEvent actionEvent) throws IOException {
        int userIndex = userList.getSelectionModel().getSelectedIndex();
        String s = userList.getItems().remove(userIndex);

        Files.deleteIfExists(Paths.get("src/photos/users/"+s+".ser"));
    }

    /**
     * Updates the list of users with files from the users folder.
     */
    private void updateUserList() {
        File filelist = new File("src/photos/users/");

        for (String s: filelist.list()) {
            User current;
            try {
                FileInputStream fileIn = new FileInputStream("src/photos/users/"+s);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                current = (User) in.readObject();
                userList.getItems().add(current.getUsername());
                in.close();
                fileIn.close();
            } catch (IOException i) {
                i.printStackTrace();
                return;
            } catch (ClassNotFoundException c) {
                System.out.println(c);
                c.printStackTrace();
                return;
            }

        }

    }

    /**
     * Getter for the mainStage field.
     * @return The main stage associated with this controller.
     */
    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Setter for the mainStage field.
     * @param mainStage The main stage associated with this controller.
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
}
