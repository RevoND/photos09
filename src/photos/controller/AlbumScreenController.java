package photos.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import photos.model.Album;
import photos.model.Dialogues;
import photos.model.Photo;
import photos.model.User;

import java.io.*;
import java.util.Date;
import java.util.Optional;

/**
 * @author Noe Duran
 * This class represents the controller for the album screen.
 */
public class AlbumScreenController {
    /**
     * the stage associated with this controller.
     */
    Stage mainStage;

    /**
     * The username label on this panel.
     */
    @FXML Label  usernameLabel;
    /**
     * The logout button.
     */
    @FXML Button logoutButton;
    /**
     * The new album button.
     */
    @FXML Button newAlbumButton;
    /**
     * The delete album button.
     */
    @FXML Button deleteAlbumButton;
    /**
     * The edit album button.
     */
    @FXML Button editAlbumButton;
    /**
     * The search button.
     */
    @FXML Button searchButton;
    /**
     * The scroll pane that holds the flowpane.
     */
    @FXML ScrollPane scrollPane;
    /**
     * The flowpane that holds the albums.
     */
    @FXML FlowPane flowPane;
    /**
     * The current user that owns this album.
     */
    private User current;

    /**
     * This method runs right before the scene using this controller loads in.
     * @param mainStage The stage associated with this controller.
     * @param username The username of the user that has just logged in.
     */
    public void start(Stage mainStage, String username) {
        setMainStage(mainStage);

        if(!username.equals("stock")) {
            try {
                FileInputStream fileIn = new FileInputStream("src/photos/users/" + username + ".ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                current = (User) in.readObject();
                in.close();
                fileIn.close();
            } catch (IOException | ClassNotFoundException i) {
                Dialogues.showErrorDialog(i);
                i.printStackTrace();
                return;
            }
        }

        usernameLabel.setText(username+"'s Albums");

        if(username.equals("stock")) {
            Album stockAlbum = new Album(null, "stock");
            try {
                addAlbum(stockAlbum, true);
            } catch (FileNotFoundException e) {
                Dialogues.showErrorDialog("Unable to add stock photo.", e);
                e.printStackTrace();
                return;
            }
            stockAlbum.addPhoto(new Photo("stock1", new Date(), new File("src/photos/resources/stock/stock1.jpg"), stockAlbum));
            stockAlbum.addPhoto(new Photo("stock2", new Date(), new File("src/photos/resources/stock/stock2.jpg"), stockAlbum));
            stockAlbum.addPhoto(new Photo("stock3", new Date(), new File("src/photos/resources/stock/stock3.jpg"), stockAlbum));
            stockAlbum.addPhoto(new Photo("stock4", new Date(), new File("src/photos/resources/stock/stock4.jpg"), stockAlbum));
            stockAlbum.addPhoto(new Photo("stock5", new Date(), new File("src/photos/resources/stock/stock5.jpg"), stockAlbum));
        } else {
            for(Album a : current.getAlbums()) {
                try {
                    addAlbum(a, true);
                } catch (FileNotFoundException e) {
                    Dialogues.showErrorDialog("Could not load album " + a.getAlbumName());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Creating a new album.
     * @param actionEvent The actionevent associated with this button press.
     * @throws FileNotFoundException If the stock image for the album cannot be found.
     */
    @FXML
    public void newAlbum(ActionEvent actionEvent) throws FileNotFoundException {
        // create new element in vbox
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("Create Album");
        userDialog.setContentText("Album Name:");

        Optional<String> maybeAlbumName = userDialog.showAndWait();
        if(maybeAlbumName.isEmpty()) return;

        Album createdAlbum = new Album(current, maybeAlbumName.get());

        addAlbum(createdAlbum, false);

        try {
            File newUserSer = new File("src/photos/users/"+current.getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(current);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }
    }

    private void addAlbum(Album createdAlbum, boolean viewOnly) throws FileNotFoundException {
        // check if album already exists
        if(!viewOnly) {
            if (current.getAlbums().contains(createdAlbum)) {
                Dialogues.showErrorDialog("Cannot have two albums of the same name.");
                return;
            }
        }

        VBox album = new VBox();
        Text albumInfo = new Text( " Album:\n" +  "  "+ createdAlbum.getAlbumName());
        albumInfo.setStyle("font-weight: bold");
        album.getChildren().addAll( albumInfo, new Text("Photos: "+ createdAlbum.getNumberPhotos()),new Text("Dates: \n09/20 - 02/90"));
        album.setSpacing(5);
        album.setStyle("-fx-border-color: black;\n" +
                "-fx-border-insets: 2;\n" +
                "-fx-border-width: 2;\n");

        album.setOnMouseClicked(e -> {
            if(!e.getButton().equals(MouseButton.PRIMARY)) return;
            // single click
            if(e.getClickCount() == 1) {
                // remove effect from all albums
                ObservableList<Node> paneChildren = flowPane.getChildren();
                for (Node child : paneChildren) {
                    if (!(child instanceof VBox)) continue;
                    VBox vboxChild = (VBox) child;
                    vboxChild.setEffect(null);
                }
                // add effect to this album
                DropShadow borderGlow = new DropShadow();
                borderGlow.setColor(Color.BLUEVIOLET);
                borderGlow.setOffsetX(0f);
                borderGlow.setOffsetY(0f);

                album.setEffect(borderGlow);
                // double click (open album)
            } else if(e.getClickCount() == 2) {
                Stage mainStage = getMainStage();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/photos/view/photoscreen.fxml"));
                Pane newPane = null;
                try {
                    newPane = loader.load();
                } catch (IOException ioException) {
                    Dialogues.showErrorDialog(ioException);
                    ioException.printStackTrace();
                    return;
                }
                PhotoScreenController psc = loader.getController();
                psc.start(mainStage, createdAlbum,current);

                mainStage.setScene(new Scene(newPane));
                mainStage.setTitle(current == null ? "stock" : current.getUsername() + "'s album: " + createdAlbum.getAlbumName());
                mainStage.show();
            }
        });

        flowPane.getChildren().add(album);
        if(!viewOnly){
            current.addAlbum(createdAlbum);
        }
    }

    /**
     * Get the index of the selected album.
     * @return The index of the selected album.
     */
    private int getSelected() {
        // check all the vbox's in the flowpane to see if one has the effect
        ObservableList<Node> paneChildren = flowPane.getChildren();

        int i = 0;
        for(Node child : paneChildren) {
            if(!(child instanceof VBox)) continue;
            VBox vboxChild = (VBox) child;
            if(vboxChild.getEffect() != null)
                return i;
            i += 1;
        }
        return -1;
    }

    /**
     * Logging out of the current user account.
     * @param ae Actionevent associated with this button press.
     * @throws IOException When the FXML associated with the new view cannot be found.
     */
    @FXML
    private void logout(ActionEvent ae) throws IOException {
        Stage mainStage = getMainStage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/view/loginScreen.fxml"));
        Pane newPane = loader.load();
        PhotosController loginScreen = loader.getController();
        loginScreen.start(mainStage);

        Scene loginScene = new Scene(newPane);

        mainStage.setScene(loginScene);
        mainStage.setTitle("Login");
        mainStage.setResizable(false);
        mainStage.show();
    }

    /**
     * Delete the selected album.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void deleteAlbum(ActionEvent actionEvent) {
        int selectedItem = getSelected();
        if(selectedItem == -1) {
            Dialogues.showErrorDialog("No albums are selected.");
            return;
        }
        flowPane.getChildren().remove(selectedItem);
        current.getAlbums().remove(selectedItem);

        try {
            File newUserSer = new File("src/photos/users/"+current.getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(current);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }
    }

    /**
     * Brings up a dialogue to help the user edit fields of the album.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void editAlbum(ActionEvent actionEvent) {
        int selectedItem = getSelected();
        if(selectedItem == -1) {
            Dialogues.showErrorDialog("No albums are selected.");
            return;
        }
        // create new element in vbox
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("New name");
        userDialog.setContentText("Album Name:");

        Optional<String> maybeAlbumName = userDialog.showAndWait();
        if(maybeAlbumName.isEmpty()) return;

        current.getAlbums().get(selectedItem).setAlbumName(maybeAlbumName.get());
        flowPane.getChildren().clear();

        try {
            File newUserSer = new File("src/photos/users/"+current.getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(current);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }

        start(mainStage,current.getUsername());

    }

    /**
     * Takes the user to a new view to help them search for photos.
     * @param actionEvent The actionevent associated with this button press.
     * @throws IOException If the FXML for the search view cannot be found or loaded.
     */
    public void searchPhotos(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/view/searchScreen.fxml"));
        Pane newPane = loader.load();
        SearchScreenController ssc = loader.getController();
        ssc.start(mainStage, current);

        mainStage.setScene(new Scene(newPane));
        mainStage.setTitle("Search");
        mainStage.show();
    }

    /**
     * Getter for the mainStage field.
     * @return The main stage associated with this controller.
     */
    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Setter for the mainStage field.
     * @param mainStage The main stage associated with this controller.
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
}
