package photos.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import photos.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Noe Duran
 * This class represents the contorller for the photos screen.
 */
public class PhotoScreenController {
    /**
     * The flowPane that holds the photos in this album.
     */
    @FXML private FlowPane flowPane;
    /**
     * The back button.
     */
    @FXML private Button backBN;
    /**
     * The text that represents the album name.
     */
    @FXML private Text albumNameText;
    /**
     * The text that represents the caption name for the selected photo.
     */
    @FXML private Text captionText;
    /**
     * The date for the selected photo.
     */
    @FXML private Text dateText;
    /**
     * The imageview on the right panel. Changes when an image is selected.
     */
    @FXML private ImageView rightPanelImage;

    @FXML private VBox tagVBox;

    /**
     * the stage associated with this controller.
     */
    Stage mainStage;

    /**
     * The album chosen with this controller.
     */
    Album album;

    private User current;

    /**
     * This method runs right before the scene using this controller loads in.
     * @param mainStage The stage associated with this controller.
     * @param album The album that has been selected.
     * @param current
     */
    public void start(Stage mainStage, Album album, User current) {
        setMainStage(mainStage);
        this.album = album;
        this.current = current;

        albumNameText.setText(album.getAlbumName());

        for(Photo p : album.getPhotoList()) addPhotoToList(p);
    }

    /**
     * Adds photo to user object and to flowpane.
     * @param actionEvent The actionevent associated with the button press.
     */
    public void addPhoto(ActionEvent actionEvent) {
        // add photo object to album
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Add picture to  \"" + album.getAlbumName() + "\"");
        File file = fileChooser.showOpenDialog(getMainStage());
        if(file == null) {
            Dialogues.showErrorDialog("Unable to get file.");
            return;
        }

        // caption
        TextInputDialog createCaption = new TextInputDialog();
        createCaption.setTitle("Create Caption");
        createCaption.setContentText("Caption: ");

        Optional<String> maybeCaption = createCaption.showAndWait();
        if(maybeCaption.isEmpty()) return;

        Photo newPhoto = new Photo(maybeCaption.get(), new Date(), file, album);
        album.addPhoto(newPhoto);
        // add photo object to photo list
        addPhotoToList(newPhoto);

        reserialize();
    }

    public void reserialize() {
        try {
            File newUserSer = new File("src/photos/users/"+album.getUser().getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(current);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }
    }

    /**
     * Add a photo to the flowpane.
     * @param p The photo to add to the flowpane.
     */
    private void addPhotoToList(Photo p) {
        // hbox
        HBox hbox = new HBox();
        hbox.setPrefHeight(60.0);
        hbox.setPrefWidth(189.0);
        hbox.setAlignment(Pos.CENTER_LEFT);
        // image view
        ImageView imv;
        Image img;
        try {
            img = new Image(new FileInputStream(p.getPhotoLocation()));
            imv = new ImageView(img);
            imv.setFitHeight(60.0);
            imv.setFitWidth(62.0);
            imv.setPickOnBounds(true);
            imv.setPreserveRatio(true);
        } catch (FileNotFoundException e) {
            Dialogues.showErrorDialog("Image could not be found. Skipping...", e);
            e.printStackTrace();
            return;
        }

        Text caption = new Text(p.getCaption());
        caption.setStrokeType(StrokeType.OUTSIDE);
        caption.setStrokeWidth(0.0);
        caption.setWrappingWidth(100.0);
        // add to hbox
        hbox.getChildren().addAll(imv, caption);

        for(Tag g: p.getPhotoTags()){
            tagVBox.getChildren().add(new Text(g.toString()));
        }

        // add to flowpane
        flowPane.getChildren().add(hbox);

        // add listener for hbox click (selection)
        hbox.setOnMouseClicked(e -> {
            if(!e.getButton().equals(MouseButton.PRIMARY)) return;
            // remove effect from all hbox's
            ObservableList<Node> paneChildren = flowPane.getChildren();
            for (Node child : paneChildren) {
                if (!(child instanceof HBox)) continue;
                HBox hboxChild = (HBox) child;
                hboxChild.setEffect(null);
            }
            // add effect to this hbox
            DropShadow borderGlow = new DropShadow();
            borderGlow.setColor(Color.BLUEVIOLET);
            borderGlow.setOffsetX(0f);
            borderGlow.setOffsetY(0f);

            hbox.setEffect(borderGlow);
            // update right pane
            rightPanelImage.setImage(img);
            captionText.setText(p.getCaption());
            dateText.setText(p.getDate().toString());
        });
    }

    /**
     * Deletes the selected photo from the album and the flowpane.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void deletePhoto(ActionEvent actionEvent) {
        int index = getSelected();
        if(index == -1) {
            Dialogues.showErrorDialog("No photo selected.");
            return;
        }
        // remove from pane
        flowPane.getChildren().remove(index);
        // from album
        album.removePhoto(index);

        try {
            File newUserSer = new File("src/photos/users/"+album.getUser().getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(current);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }

        reserialize();
    }

    /**
     * Edit the selected photo.
     * @param actionEvent Actionevent assocaited with this button press.
     */
    public void editPhoto(ActionEvent actionEvent) {
        int selectedItem = getSelected();
        if(selectedItem == -1) {
            Dialogues.showErrorDialog("No photo was selected.");
            return;
        }

        TextInputDialog recaption = new TextInputDialog();
        recaption.setTitle("New caption");
        recaption.setContentText("Caption:");

        Optional<String> maybeCaption = recaption.showAndWait();
        if(maybeCaption.isEmpty()) return;

        album.findPhoto(getSelected()).setCaption(maybeCaption.get());
        flowPane.getChildren().clear();
        captionText.setText(maybeCaption.get());


        // LEAVE THIS AS THE LAST LINE
        reserialize();

        start(mainStage,album,current);
    }

    /**
     * Get the index of the selected photo.
     * @return The index of the selected photo.
     */
    private int getSelected() {
        // check all the vbox's in the flowpane to see if one has the effect
        ObservableList<Node> paneChildren = flowPane.getChildren();

        int i = 0;
        for(Node child : paneChildren) {
            if(!(child instanceof HBox)) continue;
            HBox hboxChild = (HBox) child;
            if(hboxChild.getEffect() != null)
                return i;
            i += 1;
        }
        return -1;
    }

    /**
     * Getter for the mainStage field.
     * @return The main stage associated with this controller.
     */
    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Setter for the mainStage field.
     * @param mainStage The main stage associated with this controller.
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    /**
     * The back button. Sends back to the album screen.
     * @param actionEvent The actionevent associated with this button press.
     * @throws IOException If the FXML file could not be found or loaded.
     */
    public void back(ActionEvent actionEvent) throws IOException {
        Stage mainStage = getMainStage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/view/albumScreen.fxml"));
        Pane newPane = loader.load();
        AlbumScreenController asc = loader.getController();
        String username = album.getUser() == null ? "stock" : album.getUser().getUsername();
        asc.start(mainStage, username);

        Scene albumScene = new Scene(newPane);

        mainStage.setScene(albumScene);
        mainStage.setTitle(username + "'s Albums");
        mainStage.setResizable(false);
        mainStage.show();
    }

    /**
     * Adds a tag to the selected photo.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void addTag(ActionEvent actionEvent) {
        int selectedIdx = getSelected();
        if(selectedIdx == -1) {
            Dialogues.showErrorDialog("No photo selected.");
            return;
        }
        Photo p = album.getPhotoList().get(selectedIdx);
        // prompt user for tag
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("Add tag");
        userDialog.setContentText("Tag (format <key>=<value>):");

        String tag = null;
        do {
            if(tag != null)
                userDialog.setContentText("Wrong format, try again\nTag (format <key>=<value):");
            Optional<String> maybeTag = userDialog.showAndWait();
            if (maybeTag.isEmpty()) return;
            tag = maybeTag.get();
        } while(tag.split("=").length != 2);
        String key = tag.split("=")[0];
        String value = tag.split("=")[1];
        // add tag to photo object
        p.addTag(new Tag(key, value));


        // DO NOT TOUCH THIS LINE
        reserialize();
    }

    /**
     * Deletes a tag from the selected photo.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void deleteTag(ActionEvent actionEvent) {
        int selectedIdx = getSelected();
        if(selectedIdx == -1) {
            Dialogues.showErrorDialog("No photo selected.");
            return;
        }
        Photo p = album.getPhotoList().get(selectedIdx);
        // prompt user for tag
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("Delete tag");
        userDialog.setContentText("Tag (format <key>=<value>):");

        String tag = null;
        do {
            if(tag != null)
                userDialog.setContentText("Wrong format, try again\nTag (format <key>=<value):");
            Optional<String> maybeTag = userDialog.showAndWait();
            if (maybeTag.isEmpty()) return;
            tag = maybeTag.get();
        } while(tag.split("=").length != 2);
        String key = tag.split("=")[0];
        String value = tag.split("=")[1];
        // delete tag from photo tags
        boolean successful = p.removeTag(new Tag(key, value));
        if(!successful)
            Dialogues.showErrorDialog("Unable to find tag " + new Tag(key, value).toString() + " for this photo.");
        // update in right panel
        // TODO

        // DO NOT TOUCH THIS LINE
        reserialize();
    }

    /**
     * Copy the selected photo to another album.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void copyToAlbum(ActionEvent actionEvent) {
        int selectedIdx = getSelected();
        if(selectedIdx == -1) {
            Dialogues.showErrorDialog("No photo selected.");
            return;
        }
        Photo p = album.getPhotoList().get(selectedIdx);
        // check if the user has any other albums
        User u = album.getUser();
        if(u.getAlbums().size() <= 1) {
            Dialogues.showErrorDialog("There are no other albums to copy this photo to.");
            return;
        }
        // bring up dialogue that lets the user choose another album they own
        List<String> choices = new ArrayList<>();
        for(Album a : u.getAlbums()) {
            if(!a.getAlbumName().equals(album.getAlbumName()))
                choices.add(a.getAlbumName());
        }
        ChoiceDialog<String> dialog = new ChoiceDialog<String>(choices.get(0), choices);
        dialog.setTitle("Copy photo to another album");
        dialog.setHeaderText("Choose an album to copy this photo to");
        dialog.setContentText("Choose an album:");
        // Process input
        Optional<String> result = dialog.showAndWait();
        if(result.isEmpty()) return; // this is impossible!
        String albumName = result.get();
        // get album
        Album album = null;
        for(Album a : u.getAlbums()) {
            if(a.getAlbumName().equals(albumName))
                album = a;
        }
        if(album == null) {
            Dialogues.showErrorDialog("Album " + albumName + " could not be found."); // this can't happen!
            return;
        }
        // add photo to that album
        album.addPhoto(p);
    }

    /**
     * Move (copy + delete) the selected photo to another album.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void moveToAlbum(ActionEvent actionEvent) {
        copyToAlbum(actionEvent);
        // remove the photo from this album
        int selectedIdx = getSelected();
        if(selectedIdx == -1) {
            Dialogues.showErrorDialog("No photo selected.");
            return;
        }
        deletePhoto(actionEvent);

        reserialize();
    }
}
