package photos.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import photos.model.Dialogues;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Represents the controller for the photos view.
 */
public class PhotosController {
    /**
     * The login button.
     */
    @FXML Button loginBN;
    /**
     * The username text field.
     */
    @FXML TextField usernameTF;

    /**
     * The stage associatedd with this controller.
     */
    Stage mainStage;

    /**
     * This method runs right before the scene using this controller loads in.
     * @param mainStage The stage associated with this controller.
     */
    public void start(Stage mainStage) {
        setMainStage(mainStage);
    }

    /**
     * Submission of the username..
     * @param ae Actionevent associated with the button press.
     */
    @FXML
    private void submit(ActionEvent ae) {
        String username = usernameTF.getText();
        if(username.isBlank()) {
            Dialogues.showErrorDialog("Cannot have a blank username.");
            return;
        }

        try {
            loginAsUser(username);
        } catch (IOException e) {
            Dialogues.showErrorDialog("Exception encountered while logging in.", e);
            e.printStackTrace();
        }
    }

    /**
     * Logging in as a certain user.
     * @param username The username to log in as.
     * @throws IOException If the program is unable to find or load the following FXML.
     */
    private void loginAsUser(String username) throws IOException {
        Stage mainStage = getMainStage();

        FXMLLoader loader = new FXMLLoader();
        String windowTitle;
        Pane newPane;
        if(username.equals("admin")) {
            loader.setLocation(getClass().getResource("/photos/view/adminScreen.fxml"));
            windowTitle = "Admin";
            newPane = loader.load();
            AdminPanelController apc = loader.getController();
            apc.start(mainStage);

            mainStage.setScene(new Scene(newPane));
            mainStage.setTitle(windowTitle);
            mainStage.show();
        } else {
            File filelist = new File("src/photos/users/");
            boolean foundUserFlag = false;
            if(username.equals("stock")) {
                loader.setLocation(getClass().getResource("/photos/view/albumScreen.fxml"));
                windowTitle = username + "'s Albums";
                newPane = loader.load();

                AlbumScreenController asc = loader.getController();
                asc.start(mainStage, username);

                mainStage.setScene(new Scene(newPane));
                mainStage.setTitle(windowTitle);
                mainStage.show();
            } else {
                for (String s : Objects.requireNonNull(filelist.list())) {
                    if (username.equalsIgnoreCase(s.split("\\.")[0])) {
                        foundUserFlag = true;
                        loader.setLocation(getClass().getResource("/photos/view/albumScreen.fxml"));
                        windowTitle = username + "'s Albums";
                        newPane = loader.load();

                        AlbumScreenController asc = loader.getController();
                        asc.start(mainStage, username);

                        mainStage.setScene(new Scene(newPane));
                        mainStage.setTitle(windowTitle);
                        mainStage.show();

                        break;
                    }
                }

                if(!foundUserFlag)
                    Dialogues.showErrorDialog("User \"" + username + "\" could not be found.");
            }

        }
    }

    /**
     * Getter for the mainStage field.
     * @return The main stage associated with this controller.
     */
    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Setter for the mainStage field.
     * @param mainStage The main stage associated with this controller.
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
}
