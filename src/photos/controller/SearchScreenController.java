package photos.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import photos.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

/**
 * Represents the controller for the search screen.
 */
public class SearchScreenController {
    /**
     * The stage associated with this controller.
     */
    Stage mainStage;

    /**
     * The user performing the search.
     */
    User user;

    /**
     * The create album button.
     */
    @FXML
    Button createAlbumButton;
    /**
     * The back button.
     */
    @FXML
    Button backButton;

    /**
     * The search button.
     */
    @FXML
    Button searchButton;

    /**
     * The scroll pane for the photos.
     */
    @FXML
    ScrollPane photoScrollPane;

    @FXML
    FlowPane photoFlowPane;

    /**
     * The query for tags.
     */
    @FXML TextField tagQueryTF;

    /**
     * The query for dates.
     */
    @FXML TextField dateQueryTF;

    /**
     * Store the results in an arraylist.
     */
    ArrayList<Photo> searchResults;

    /**
     * This method runs right before the scene using this controller loads in.
     * @param mainStage The stage associated with this controller.
     */
    public void start(Stage mainStage, User user) {
        setMainStage(mainStage);
        this.user = user;
        this.searchResults = new ArrayList<>();
    }

    /**
     * Pressing the back button.
     * @param actionEvent The actionevent associated with this button press.
     * @throws IOException If the FXML file cannot be loaded or found.
     */
    public void backPress(ActionEvent actionEvent) throws IOException {
        Stage mainStage = getMainStage();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/photos/view/albumScreen.fxml"));
        Pane newPane = loader.load();
        AlbumScreenController asc = loader.getController();
        asc.start(mainStage, user.getUsername());

        mainStage.setScene(new Scene(newPane));
        mainStage.setTitle(user.getUsername() + "'s Albums");
        mainStage.show();
    }

    /**
     * Searching the photos.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void searchPhotos(ActionEvent actionEvent) {
        // flush old searches
        while(photoFlowPane.getChildren().size() > 0)
            photoFlowPane.getChildren().remove(0);
        while(searchResults.size() > 0)
            searchResults.remove(0);
        // start new search
        String tagQuery = tagQueryTF.getText();
        String dateQuery = dateQueryTF.getText();
        if(tagQuery.isBlank() && dateQuery.isBlank()) {
            Dialogues.showErrorDialog("One of the fields must be filled out to search.");
            return;
        } else if(!tagQuery.isBlank() && !dateQuery.isBlank()) {
            Dialogues.showErrorDialog("Can only search by one criteria. Please delete the content in one of the TextFields.");
            return;
        } else if(dateQuery.isBlank()) {
            // add all photos that match the tag query
            if(!tagQuery.contains(" AND ") && !tagQuery.contains(" OR ")) {
                if(tagQuery.split("=").length != 2) {
                    Dialogues.showErrorDialog("Illegal tag format (<key>=<value>). Try again.");
                    return;
                }
                String tagKey = tagQuery.split("=")[0];
                String tagValue = tagQuery.split("=")[1];
                Tag searchTag = new Tag(tagKey, tagValue);
                for(Album a : user.getAlbums()) {
                    for (Photo p : a.getPhotoList()) {
                        System.out.print("Looking at " + p.getCaption() + ", tags: ");
                        for(Tag t : p.getPhotoTags())
                            System.out.print(t + ", ");
                        if(p.hasTag(searchTag))
                            addPhoto(p);
                    }
                }
            } else if(tagQuery.contains(" AND ")) {
                if(tagQuery.split(" AND ").length != 2) {
                    Dialogues.showErrorDialog("Illegal tag format (<key>=<value> AND <key>=<value>). Try again.");
                    return;
                }
                String tagQueryOne = tagQuery.split(" AND ")[0];
                String tagQueryTwo = tagQuery.split(" AND ")[1];

                String tagKeyOne = tagQueryOne.split("=")[0];
                String tagValueOne = tagQueryOne.split("=")[1];
                String tagKeyTwo = tagQueryTwo.split("=")[0];
                String tagValueTwo = tagQueryTwo.split("=")[1];

                Tag searchTagOne = new Tag(tagKeyOne, tagValueOne);
                Tag searchTagTwo = new Tag(tagKeyTwo, tagValueTwo);

                for(Album a : user.getAlbums()) {
                    for (Photo p : a.getPhotoList()) {
                        if(p.hasTag(searchTagOne) && p.hasTag(searchTagTwo))
                            addPhoto(p);
                    }
                }
            } else if(tagQuery.contains(" OR ")) {
                if(tagQuery.split(" OR ").length != 2) {
                    Dialogues.showErrorDialog("Illegal tag format (<key>=<value> OR <key>=<value>). Try again.");
                    return;
                }
                String tagQueryOne = tagQuery.split(" OR ")[0];
                String tagQueryTwo = tagQuery.split(" OR ")[1];

                String tagKeyOne = tagQueryOne.split("=")[0];
                String tagValueOne = tagQueryOne.split("=")[1];
                String tagKeyTwo = tagQueryTwo.split("=")[0];
                String tagValueTwo = tagQueryTwo.split("=")[1];

                Tag searchTagOne = new Tag(tagKeyOne, tagValueOne);
                Tag searchTagTwo = new Tag(tagKeyTwo, tagValueTwo);

                for(Album a : user.getAlbums()) {
                    for (Photo p : a.getPhotoList()) {
                        if(p.hasTag(searchTagOne) || p.hasTag(searchTagTwo))
                            addPhoto(p);
                    }
                }
            } else {
                Dialogues.showErrorDialog("Illegal input.");
                return;
            }
        } else if(tagQuery.isBlank()) {
            if(dateQuery.split(" - ").length != 2) {
                Dialogues.showErrorDialog("Illegal date input.");
                return;
            }
            String firstDateStr = dateQuery.split(" - ")[0];
            String secondDateStr = dateQuery.split(" - ")[1];

            Date firstDate = new Date(firstDateStr);
            Date secondDate = new Date(secondDateStr);

            for(Album a : user.getAlbums()) {
                for (Photo p : a.getPhotoList()) {
                    Date photoDate = p.getDate();
                    if(photoDate.after(firstDate) && photoDate.before(secondDate))
                        addPhoto(p);
                }
            }
        }
    }

    public void addPhoto(Photo p) {
        // make new hbox
        HBox hbox = new HBox();
        hbox.setPrefHeight(60.0);
        hbox.setPrefWidth(189.0);
        hbox.setAlignment(Pos.CENTER_LEFT);
        // image view
        ImageView imv;
        Image img;
        try {
            img = new Image(new FileInputStream(p.getPhotoLocation()));
            imv = new ImageView(img);
            imv.setFitHeight(60.0);
            imv.setFitWidth(62.0);
            imv.setPickOnBounds(true);
            imv.setPreserveRatio(true);
        } catch (FileNotFoundException e) {
            Dialogues.showErrorDialog("Image could not be found. Skipping...", e);
            e.printStackTrace();
            return;
        }
        // caption
        Text caption = new Text(p.getCaption());
        caption.setStrokeType(StrokeType.OUTSIDE);
        caption.setStrokeWidth(0.0);
        caption.setWrappingWidth(100.0);
        // album
        Text album = new Text("Album: " + p.getAlbum().getAlbumName());
        // add to hbox
        hbox.getChildren().addAll(imv, caption, album);
        // add to flowpane
        photoFlowPane.getChildren().add(hbox);
        // add to search results
        searchResults.add(p);
    }

    /**
     * Creating a new album.
     * @param actionEvent The actionevent associated with this button press.
     */
    public void createAlbum(ActionEvent actionEvent) {
        TextInputDialog userDialog = new TextInputDialog();
        userDialog.setTitle("Create Album");
        userDialog.setContentText("Album Name:");

        Optional<String> maybeAlbumName = userDialog.showAndWait();
        if(maybeAlbumName.isEmpty()) return;

        Album newAlbum;
        try {
            newAlbum = new Album(user, maybeAlbumName.get());
        } catch(IllegalArgumentException iae) {
            Dialogues.showErrorDialog("Cannot have two of the same album name.", iae);
            return;
        }
        for(Photo p : searchResults)
            newAlbum.addPhoto(p);

        user.addAlbum(newAlbum);


        try {
            File newUserSer = new File("src/photos/users/"+user.getUsername()+".ser");
            FileOutputStream fileOut =
                    new FileOutputStream(newUserSer);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(user);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            Dialogues.showErrorDialog("Cannot be created: ", i);
        }
    }

    /**
     * Getter for the mainStage field.
     * @return The main stage associated with this controller.
     */
    public Stage getMainStage() {
        return mainStage;
    }

    /**
     * Setter for the mainStage field.
     * @param mainStage The main stage associated with this controller.
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }
}
