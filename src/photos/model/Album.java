package photos.model;


import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * @author Noe Duran
 * Represents an album for a user which contains many photos and has a unique name per-user.
 */
public class Album implements java.io.Serializable {
    /**
     * User that owns this album
     */
    private User user;
    /**
     * Name of this album (unique per-user).
     */
    private String albumName;
    /**
     * List of all photos in this album.
     */
    private ArrayList<Photo> photoList;

    /**
     * List of all photos in this album.
     */
    private String earliestDate;

    /**
     * List of all photos in this album.
     */
    private String latestDate;

    /**
     * Create an album. Needs the user that created the album along with its name.
     * @param user The user that created this album.
     * @param albumName The name of this album.
     * @throws IllegalArgumentException Thrown for null arguments or if the album name is already used by this user.
     */
    public Album(User user, String albumName) throws IllegalArgumentException {
        if(albumName == null) throw new IllegalArgumentException("Null arguments.");
        if(user != null) {
            for (Album a : user.getAlbums()) {
                if (a.getAlbumName().equals(albumName))
                    throw new IllegalArgumentException("Can't have two albums of the same name.");
            }
        }
        this.albumName = albumName;
        this.user = user;
        this.photoList = new ArrayList<>();
    }

    /**
     * Setter for the albumName field.
     * @param newName The new name for the album
     */
    public void setAlbumName(String newName){
        this.albumName = newName;
    }

    /**
     * Getter for the albumName field.
     * @return The name of this album.
     */
    public String getAlbumName(){
        return this.albumName;
    }

    /**
     * Add photo to this album.
     * @param photo The photo to be added to this album.
     */
    public void addPhoto(Photo photo){
        //Check if photo exist first
        photoList.add(photo);
    }

    /**
     * Removes a photo from this album.
     * @param index The index of the photo to remove.
     */
    public void removePhoto(int index){
        photoList.remove(index);
    }

    /**
     * Removes a photo from this album.
     * @param index The index of the photo to remove.
     */
    public Photo findPhoto(int index){
        return photoList.get(index);
    }

    /**
     * Gets number of photos in album
     * @return Number of photos
     */
    public int getNumberPhotos(){
        return photoList.size();
    }

    /**
     * Equality check with another album.
     * @param o The object to compare this photo to.
     * @return Whether this album has the same name as another album.
     */
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Album)) return false;
        Album a = (Album) o;
        return getAlbumName().equals(a.getAlbumName());
    }

    /**
     * Getter for the user field.
     * @return The user that created this album.
     */
    public User getUser() {
        return user;
    }

    /**
     * Getter for the earliest photo date.
     * @return The date of the photo.
     */
    public String getEarliestDate() {
        return earliestDate;
    }
    /**
     * Setter for the earliest photo date.
     @param earliestDate The new name for the album
     */

    public void setEarliestDate(String earliestDate) {
        this.earliestDate = earliestDate;
    }

    /**
     * Getter for the latest photo date.
     * @return The date of the photo.
     */

    public String getLatestDate() {
        return latestDate;
    }


    /**
     * Setter for the latest photo date.
      @param latestDate The new name for the album
     */

    public void setLatestDate(String latestDate) {
        this.latestDate = latestDate;
    }

    /**
     * Getter for the photoList field.
     * @return All the photos in this album.
     */
    public ArrayList<Photo> getPhotoList() {
        return photoList;
    }
}