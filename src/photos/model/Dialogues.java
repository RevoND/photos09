package photos.model;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * @author Arjun Srivastav
 * Helper class with some utilities for dialogues.
 */
public class Dialogues implements java.io.Serializable {
    // helpers (errors)

    /**
     * Shows an error dialogue with a given message and type of the error.
     * @param message Message to be displayed to the user.
     * @param errorType The type of error that happened.
     */
    public static void showErrorDialog(String message, String errorType) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(errorType == null || errorType.equals("") ? "Error encountered!" : "Error encountered: " + errorType);
        alert.setHeaderText("An error was encountered in the program.");
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Overloaded version of showErrorDialog. Supplies the name of the exception to errorType.
     * @param message Message to be displayed to the user.
     * @param e The exception whose name will be passed to the error dialogue.
     */
    public static void showErrorDialog(String message, Exception e) { showErrorDialog(message, e.getClass().getName()); }

    /**
     * Overloaded version of showErrorDialog. Only needs a message.
     * @param message Message to be displayed to the user.
     */
    public static void showErrorDialog(String message) { showErrorDialog(message, ""); }
    /**
     * Overloaded version of showErrorDialog. Supplies the name of the exception to errorType. No message needed.
     * @param e The exception whose name will be passed to the error dialogue.
     */
    public static void showErrorDialog(Exception e) { showErrorDialog(e.getClass().getName() + " exception was encountered.", e); }

    // pop-up dialogue

    /**
     * Pop-up dialog for confirming a user wanted to execute an action.
     * @param message The message to show the user when the confirmation dialogue pops up.
     * @return Whether or not the action was confirmed.
     */
    public static boolean showConfirmationDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(message);
        alert.setContentText("Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
        if(result.isEmpty()) return false;
        return result.get() == ButtonType.OK;
    }
}