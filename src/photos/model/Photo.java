package photos.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Noe Duran
 * Represents a photo from a single album which may have many tags.
 */
public class Photo implements java.io.Serializable {
    /**
     * The album that this photo belongs to.
     */
    public Album album;

    /**
     * The caption for this photo.
     */
    public String caption;
    /**
     * The date the photo was created.
     */
    public Date date;
//    public Photo thumbnailPhoto;
    /**
     * A list of tags associated with the photo.
     */
    public ArrayList<Tag> photoTags;
    /**
     * The location of the file represented as a Java File object.
     */
    public File photoLocation;

    /**
     * Creating a photo.
     * @param caption The caption for the photo.
     * @param date The date the photo was made.
     * @param photoLocation The location of the photo on the user's filesystem.
     */
    public Photo(String caption, Date date, File photoLocation, Album album){
        this.caption = caption;
        this.date = date;
        this.photoTags = new ArrayList<>();
//        this.thumbnailPhoto = thumbnailPhoto;
        this.photoLocation = photoLocation;
        this.album = album;
    }

    /**
     * Getter for the photoLocation field.
     * @return The location of the photo.
     */
    public File getPhotoLocation() {
        return photoLocation;
    }

    /**
     * Getter for the caption field.
     * @return The caption for the photo.
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Setter for the caption field
     * @param caption The new caption for the photo.
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * Getter for the date field.
     * @return The date the photo was created.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter for the date field
     * @param date The date the photo was created.
     */
    public void setDate(Date date) {
        this.date = date;
    }

//    public Photo getThumbnailPhoto() {
//        return thumbnailPhoto;
//    }

    /**
     * Getter for the photoTags field.
     * @return A list for the tags for this photo.
     */
    public ArrayList<Tag> getPhotoTags() {
        return photoTags;
    }

    /**
     * Setter for the photoTags field.
     * @param photoTags A new list for the tags for the this photo.
     */
    public void setPhotoTags(ArrayList<Tag> photoTags) {
        this.photoTags = photoTags;
    }

    /**
     * Add a single tag to the photo.
     * @param t The new tag to add.
     */
    public void addTag(Tag t) {
        photoTags.add(t);
    }

    /**
     * Removes a single tag from the photo.
     * @param t The tag to remove.
     * @return Whether or not a matching tag was found and removed.
     */
    public boolean removeTag(Tag t) {
        return photoTags.remove(t);
    }

    /**
     * Check if a photo has a tag.
     * @param t Tag to search for.
     * @return Whether the photo has the tag.
     */
    public boolean hasTag(Tag t) {
        return photoTags.contains(t);
    }

    /**
     * Getter for the album field.
     * @return Return the album the photo is contained in.
     */
    public Album getAlbum() {
        return album;
    }
}
