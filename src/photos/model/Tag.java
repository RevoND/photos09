package photos.model;

/**
 * @author Noe Duran
 * Represents a tag on a photo.
 */
public class Tag implements java.io.Serializable{
    /**
     * The name of the tag (key).
     */
    public String tagName;
    /**
     * The value of the tag.
     */
    public String tagValue;

    /**
     * Create a tag using a key and value.
     * @param tagName Name of the tag.
     * @param tagValue Value of the tag.
     * @throws IllegalArgumentException If either argument is null.
     */
    public Tag(String tagName, String tagValue) throws IllegalArgumentException {
        if(tagName == null || tagValue == null) throw new IllegalArgumentException("Null argument");
        this.tagName = tagName;
        this.tagValue = tagValue;
    }

    /**
     * Getter for the tagName field.
     * @return The name of the tag.
     */
    public String getTagName(){
        return this.tagName;
    }

    /**
     * Getter for the tagValue field.
     * @return The value of the tag.
     */
    public String getTagValue() {
        return tagValue;
    }
    /**
     * Returns the string representation of the tag.
     * @return The string representation of the tag (key=value).
     */
    @Override
    public String toString() {
        return this.tagName + "=" + this.tagValue;
    }

    /**
     * Tells whether or not two tags are equivalent (same key and value).
     * @param o The tag to compare this tag to.
     * @return Whether or not two tags are the same.
     */
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Tag)) return false;
        Tag t = (Tag) o;

        return tagName.equals(t.getTagName()) && tagValue.equals(t.getTagValue());
    }
}
