package photos.model;

import photos.model.Album;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * @author Noe Duran
 * Represents a user of the Photos app.
 */
public class User implements java.io.Serializable {
    /**
     * The username of this user.
     */
    private String username;
    /**
     * All the albums this user owns.
     */
    public ArrayList<Album> albums;

    /**
     * Construct a user with just a username. Initializes an empty list for the albums.
     * @param username The username of this user.
     */
    public User(String username){
        this.username = username;
        this.albums = new ArrayList<Album>();
    }

    /**
     * Getter for the username field.
     * @return This user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Getter for the albums field.
     * @return All the albums this user owns.
     */
    public ArrayList<Album> getAlbums() {
        return albums;
    }

    /**
     * Adds an album for the user.
     * @param album The new album to be added to the user's album list.
     */
    public void addAlbum(Album album) {
        this.albums.add(album);
    }
}
